import sys
import os

fname = sys.argv[1]
wordfile = sys.argv[2]
wordstem = sys.argv[3]
dim = 300
data = '../data/data-time-plot/ppdb-100k.txt'

LC = [1E-3,1E-4,1E-5,1E-6]
LW = [1E-5,1E-6,1E-7,1E-8]
batchsize = [25,50,100]
margins = [0.4, 0.6, 0.8]
samplingtypes = ['MAX','MIX']

cmd = "sh train1.sh -wordstem {0} -wordfile {1} -outfile {2} \
-hiddensize 300 -dataf {3} \
-save False -nntype proj -evaluate True -epochs 1".format(wordstem,wordfile,fname,data)

for i in batchsize:
    for k in margins:
        for l in samplingtypes:
            for j in LW:
                next_cmd = cmd + " -batchsize {0} -LW {1} -margin {2} -samplingtype {3} -eta 0.05 -learner adagrad -nntype word".format(i,j,k,l)
                print next_cmd

for i in batchsize:
    for k in margins:
        for l in samplingtypes:
            for m in LC:
                for j in LW:
                    next_cmd = cmd + " -batchsize {0} -LW {1} -margin {2} -samplingtype {3} -LC {4} -eta 0.05 -learner adagrad -mult_side left -nntype weighted_add".format(i,j,k,l,m)
                    print next_cmd
                    next_cmd = cmd + " -batchsize {0} -LW {1} -margin {2} -samplingtype {3} -LC {4} -eta 0.05 -learner adagrad -mult_side right -nntype weighted_add".format(i,j,k,l,m)
                    print next_cmd

for i in batchsize:
    for k in margins:
        for l in samplingtypes:
            for m in LC:
                for j in LW:
                    next_cmd = cmd + " -batchsize {0} -LW {1} -margin {2} -samplingtype {3} -LC {4} -eta 0.05 -learner adagrad -nntype two_weighted_add".format(i,j,k,l,m)
                    print next_cmd
