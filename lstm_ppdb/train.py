from utils import getWordmap
from params import params
from utils import getData
from word_model import word_model
from matrix_model import matrix_model
from weighted_add_model import weighted_add_model
from two_weighted_add_model import two_weighted_add_model
import lasagne
import random
import numpy as np
import sys
import argparse


def str2bool(v):
    if v is None:
        return False
    if v.lower() in ("yes", "true", "t", "1"):
        return True
    if v.lower() in ("no", "false", "f", "0"):
        return False
    raise ValueError('A type that was supposed to be boolean is not boolean.')


def learner2bool(v):
    if v is None:
        return lasagne.updates.adagrad
    if v.lower() == "adagrad":
        return lasagne.updates.adagrad
    if v.lower() == "adam":
        return lasagne.updates.adam
    raise ValueError('A type that was supposed to be a learner is not.')


random.seed(1)
np.random.seed(1)

params = params()

parser = argparse.ArgumentParser()
parser.add_argument("-LW", help="Regularization on Words", type=float)
parser.add_argument("-LC", help="Regularization on Parameters", type=float)
parser.add_argument("-outfile", help="Output file name")
parser.add_argument("-batchsize", help="Size of batch", type=int)
parser.add_argument("-hiddensize", help="Size of input", type=int)
parser.add_argument("-wordfile", help="Word file to be read in.")
parser.add_argument("-wordstem", help="Nickname of word embeddings used.")
parser.add_argument("-save", help="Whether to pickle the model.")
parser.add_argument("-dataf", help="Training data file.")
parser.add_argument("-margin", help="Regularization on Words", type=float)
parser.add_argument("-samplingtype", help="Type of Sampling used.")
parser.add_argument("-evaluate", help="Whether to evaluate the model during training.")
parser.add_argument("-epochs", help="Number of epochs in training.", type=int)
parser.add_argument("-fraction", help="Percent of training examples to use.", type=float)
parser.add_argument("-clip", help="float to indicate grad cutting. Use 0 for no clipping.",type=int)
parser.add_argument("-eta", help="learning rate",type=float)
parser.add_argument("-learner", help="Either AdaGrad or Adam")
parser.add_argument("-nntype", help="Either matrix, word, weight_add")
parser.add_argument("-mult_side", help="Either right or left")


args = parser.parse_args()

params.LW = args.LW
params.LC = args.LC
params.outfile = args.outfile
params.batchsize = args.batchsize
params.hiddensize = args.hiddensize
params.wordfile = args.wordfile
params.wordstem = args.wordstem
params.save = str2bool(args.save)
params.dataf = args.dataf
params.margin = args.margin
params.type = args.samplingtype
params.epochs = args.epochs
params.evaluate = str2bool(args.evaluate)
params.learner = learner2bool(args.learner)
params.mult_side = args.mult_side

if args.eta:
    params.eta = args.eta

if args.clip:
    params.clip = args.clip
    if params.clip == 0:
        params.clip = None

(words, We) = getWordmap(params.wordfile)

#D = 400
#We = np.random.random((We.shape[0],D))

examples = getData(params.dataf, words)

model = None

print sys.argv

if args.nntype == "word":
    model = word_model(We, params)
elif args.nntype == "matrix":
    model = matrix_model(We, params, int(np.sqrt(D)))
elif args.nntype == "weighted_add":
    model = weighted_add_model(We, params)
elif args.nntype == "two_weighted_add":
    model = two_weighted_add_model(We, params)
model.train(examples, words, params)