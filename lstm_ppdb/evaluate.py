from scipy.spatial.distance import cosine
from scipy.stats import spearmanr
from scipy.stats import pearsonr
from utils import lookupIDX
import numpy as np

def getSeqs(p1,p2,model,words):
    p1 = p1.split()
    p2 = p2.split()
    X1 = []
    X2 = []
    for i in p1:
        X1.append(lookupIDX(words,i))
    for i in p2:
        X2.append(lookupIDX(words,i))
    return X1, X2

def getScore(p1,p2,model,words):
    p1 = p1.split()
    p2 = p2.split()
    X1 = []
    X2 = []
    for i in p1:
        X1.append(lookupIDX(words,i))
    for i in p2:
        X2.append(lookupIDX(words,i))
    seqs = [X1]
    x1,m1 = model.prepare_data(seqs)
    seqs = [X2]
    x2,m2 = model.prepare_data(seqs)
    scores = model.scoring_function(x1,x2,m1,m2)
    scores = np.squeeze(scores)
    return float(scores)

def getCorrelation(model,words,f):
    f = open(f,'r')
    lines = f.readlines()
    preds = []
    golds = []
    seq1 = []
    seq2 = []
    for i in lines:
        i = i.split("\t")
        p1 = i[0]; p2 = i[1]; score = float(i[2])
        X1, X2 = getSeqs(p1,p2,model,words)
        seq1.append(X1)
        seq2.append(X2)
        golds.append(score)
    x1,m1 = model.prepare_data(seq1)
    x2,m2 = model.prepare_data(seq2)
    scores = model.scoring_function(x1,x2,m1,m2)
    #scores = model.scoring_function(x1,x2)
    preds = np.squeeze(scores)
    return pearsonr(preds,golds)[0], spearmanr(preds,golds)[0]

def evaluate(model,words,file,out):
    p,s = getCorrelation(model,words,file)
    return p,s

def evaluate_all(model,words):
    prefix = "../datasets_tokenized/"
    parr = []; sarr = []; farr = []

    farr = ["FNWN2013",#0
            "JHUppdb",#1
            "MSRpar2012",#2
            "MSRvid2012",#3
            "OnWN2012",#4
            "OnWN2013",#5
            "OnWN2014",#6
            "SMT2013",#7
            "SMTeuro2012",#8
            "SMTnews2012",#9
            "anno-dev",#10
            "anno-test",#11
            "answer-forum2015",#12
            "answer-student2015",#13
            "belief2015",#14
            "bigram-jn",#15
            "bigram-nn",#16
            "bigram-vn",#17
            "deft-forum2014",#18
            "deft-news2014",#19
            "headline2013",#20
            "headline2014",#21
            "headline2015",#22
            "images2014",#23
            "images2015",#24
            "sicktest",#25
            "tweet-news2014",#26
            "twitter"]#27

    for i in farr:
        p,s = getCorrelation(model,words,prefix+i)
        parr.append(p); sarr.append(s)

    s = ""
    for i,j,k in zip(parr,sarr,farr):
        s += str(i)+" "+str(j)+" "+k+" | "

    n = parr[2]+ parr[3]+parr[4]+parr[8]+parr[9]
    n = n/5.
    s += str(n)+" 2012-average | "

    n = parr[0]+ parr[5]+parr[7]+parr[20]
    n = n/4.
    s += str(n)+" 2013-average | "

    n = parr[6]+ parr[18]+parr[19]+parr[21]+parr[23]+parr[26]
    n = n/6.
    s += str(n)+" 2014-average | "

    n = parr[12]+ parr[13]+parr[14]+parr[22]+parr[24]
    n = n/5.
    s += str(n)+" 2015-average | "

    print s


