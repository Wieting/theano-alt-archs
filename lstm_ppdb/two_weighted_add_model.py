import theano
import numpy as np
from theano import tensor as T
import lasagne
from theano import config
import pdb
import cPickle
import utils
import time
import sys
from evaluate import evaluate_all

def checkIfQuarter(idx,n):
    #print idx, n
    if idx==round(n/4.) or idx==round(n/2.) or idx==round(3*n/4.):
        return True
    return False

class two_weighted_add_model(object):

    #takes list of seqs, puts them in a matrix
    #returns matrix of seqs and mask
    def prepare_data(self, list_of_seqs):
        lengths = [len(s) for s in list_of_seqs]
        n_samples = len(list_of_seqs)
        maxlen = np.max(lengths)
        x = np.zeros((n_samples, maxlen)).astype('int32')
        x_mask = np.zeros((n_samples, maxlen)).astype('int32')
        for idx, s in enumerate(list_of_seqs):
            x[idx, :lengths[idx]] = s
            x_mask[idx, :lengths[idx]] = 1
        x_mask = np.asarray(x_mask,dtype='int32')
        return x, x_mask

    def saveParams(self, fname):
        f = file(fname, 'wb')
        cPickle.dump(self.all_params, f, protocol=cPickle.HIGHEST_PROTOCOL)
        f.close()

    def get_minibatches_idx(self, n, minibatch_size, shuffle=False):
        idx_list = np.arange(n, dtype="int32")

        if shuffle:
            np.random.shuffle(idx_list)

        minibatches = []
        minibatch_start = 0
        for i in range(n // minibatch_size):
            minibatches.append(idx_list[minibatch_start:
                                    minibatch_start + minibatch_size])
            minibatch_start += minibatch_size

        if (minibatch_start != n):
            # Make a minibatch out of what is left
            minibatches.append(idx_list[minibatch_start:])

        return zip(range(len(minibatches)), minibatches)

    def getpairs(self, batch, params):
        #batch is list of tuples
        g1 = []; g2 = []
        for i in batch:
            g1.append(i[0].embeddings)
            g2.append(i[1].embeddings)

        g1x, g1mask = self.prepare_data(g1)
        g2x, g2mask = self.prepare_data(g2)

        embg1 = self.feedforward_function(g1x, g1mask)
        embg2 = self.feedforward_function(g2x, g2mask)

        #print embg1
        #print embg1.shape

        #update representations
        for idx,i in enumerate(batch):
            i[0].representation = embg1[idx,:]
            i[1].representation = embg2[idx,:]

        #pairs = utils.getPairs(batch, params.type)
        pairs = utils.getPairsFast(batch, params.type)
        p1 = []; p2 = []
        for i in pairs:
            p1.append(i[0].embeddings)
            p2.append(i[1].embeddings)

        p1x, p1mask = self.prepare_data(p1)
        p2x, p2mask = self.prepare_data(p2)

        return (g1x,g1mask,g2x,g2mask,p1x,p1mask,p2x,p2mask)

    def mult_layer(self,idx,mask,We,Wrnn1,Wrnn2):

        X = We[idx]
        WX1 = Wrnn1[idx]
        WX2 = Wrnn2[idx]
        mask = T.sum(mask, axis = 1)

        def recurrence(x_t, wx1_t, wx2_t, h_tm1):
            representation = h_tm1*wx1_t + x_t*wx2_t
            return representation

        def outer_loop(x,wx1,wx2,m):
            rep, _ = theano.scan(fn=recurrence,
                             sequences=[x,wx1,wx2],
                             outputs_info=[T.zeros_like(We[0,:])],
                             n_steps=m)
            return rep[-1]

        out, _ = theano.scan(outer_loop, sequences=[X,WX1,WX2,mask], n_steps=X.shape[0])
        out = T.stack(out)
        return T.reshape(out,(out.shape[1],out.shape[2]))
        #return X


    def __init__(self, We_initial, params):

        #params
        initial_We = theano.shared(np.asarray(We_initial, dtype = config.floatX))
        We = theano.shared(np.asarray(We_initial, dtype = config.floatX))

        #parameters
        Wrnn1 = np.ones(We_initial.shape)
        Wrnn1 = theano.shared(np.asarray(Wrnn1, dtype = config.floatX))
        initial_Wrnn1 = theano.shared(np.asarray(Wrnn1.get_value(), dtype = config.floatX))

        Wrnn2 = np.ones(We_initial.shape)
        Wrnn2 = theano.shared(np.asarray(Wrnn2, dtype = config.floatX))
        initial_Wrnn2 = theano.shared(np.asarray(Wrnn2.get_value(), dtype = config.floatX))

        #symbolic params
        g1batchindices = T.imatrix(); g2batchindices = T.imatrix()
        p1batchindices = T.imatrix(); p2batchindices = T.imatrix()
        g1mask = T.imatrix(); g2mask = T.imatrix()
        p1mask = T.imatrix(); p2mask = T.imatrix()

        embg1 = self.mult_layer(g1batchindices,g1mask,We,Wrnn1,Wrnn2)
        embg2 = self.mult_layer(g2batchindices,g2mask,We,Wrnn1,Wrnn2)
        embp1 = self.mult_layer(p1batchindices,p1mask,We,Wrnn1,Wrnn2)
        embp2 = self.mult_layer(p2batchindices,p2mask,We,Wrnn1,Wrnn2)

        #objective function
        g1g2 = (embg1*embg2).sum(axis=1)
        g1g2norm = T.sqrt(T.sum(embg1**2,axis=1)) * T.sqrt(T.sum(embg2**2,axis=1))
        g1g2 = g1g2 / g1g2norm

        p1g1 = (embp1*embg1).sum(axis=1)
        p1g1norm = T.sqrt(T.sum(embp1**2,axis=1)) * T.sqrt(T.sum(embg1**2,axis=1))
        p1g1 = p1g1 / p1g1norm

        p2g2 = (embp2*embg2).sum(axis=1)
        p2g2norm = T.sqrt(T.sum(embp2**2,axis=1)) * T.sqrt(T.sum(embg2**2,axis=1))
        p2g2 = p2g2 / p2g2norm

        costp1g1 = params.margin - g1g2 + p1g1
        costp1g1 = costp1g1*(costp1g1 > 0)

        costp2g2 = params.margin - g1g2 + p2g2
        costp2g2 = costp2g2*(costp2g2 > 0)

        cost = costp1g1 + costp2g2

        word_reg = 0.5*params.LW*lasagne.regularization.l2(We-initial_We)
        mat_reg1 = 0.5*params.LC*lasagne.regularization.l2(Wrnn1-initial_Wrnn1)
        mat_reg2 = 0.5*params.LC*lasagne.regularization.l2(Wrnn2-initial_Wrnn2)
        cost = T.mean(cost) + word_reg + mat_reg1 + mat_reg2

        #feedforward
        self.feedforward_function = theano.function([g1batchindices, g1mask], embg1)
        self.cost_function = theano.function([g1batchindices, g2batchindices, p1batchindices, p2batchindices,
                                              g1mask, g2mask, p1mask, p2mask], cost)

        prediction = g1g2
        temp1 = T.matrix(); temp2 = T.matrix()
        self.scoring_function = theano.function([g1batchindices, g2batchindices, g1mask, g2mask],prediction)

        self.allparams = [We]
        #updates
        grads = theano.gradient.grad(cost, [We,Wrnn1,Wrnn2])
        if params.clip:
            grads = [lasagne.updates.norm_constraint(grad, params.clip, range(grad.ndim)) for grad in grads]
        updates = params.learner(grads, [We,Wrnn1,Wrnn2], params.eta)
        self.train_function = theano.function([g1batchindices, g2batchindices, p1batchindices, p2batchindices,
                                               g1mask, g2mask, p1mask, p2mask],
                                              cost, updates=updates)



            #trains parameters
    def train(self,data,words,params):
        start_time = time.time()
        #evaluate_all(self,words)

        counter = 0
        try:
            for eidx in xrange(params.epochs):

                # Get new shuffled index for the training set.
                kf = self.get_minibatches_idx(len(data), params.batchsize, shuffle=True)
                uidx = 0
                for _, train_index in kf:

                    uidx += 1

                    batch = [data[t] for t in train_index]
                    for i in batch:
                        i[0].populate_embeddings(words)
                        i[1].populate_embeddings(words)

                    t1 = time.time()
                    (g1x,g1mask,g2x,g2mask,p1x,p1mask,p2x,p2mask) = self.getpairs(batch, params)
                    t2 = time.time()
                    #utils.analyze_pairs(g1x,g2x,p1x,p2x)
                    #print "pairing time: "+str(t2-t1)

                    #X = self.feedforward_function(g1x,g1mask)
                    #self.testFunction(g1x,g1mask,X)
                    #print X.shape

                    #print g1x, g1mask, g2x, g2mask, p1x, p1mask, p2x, p2mask

                    t1 = time.time()
                    cost = self.train_function(g1x, g2x, p1x, p2x, g1mask, g2mask, p1mask, p2mask)
                    t2 = time.time()
                    #print "cost time: "+str(t2-t1)

                    if np.isnan(cost) or np.isinf(cost):
                        print 'NaN detected'

                    if(checkIfQuarter(uidx,len(kf))):
                        if(params.save):
                            counter += 1
                            self.saveParams(params.outfile+str(counter)+'.pickle')
                        if(params.evaluate):
                            evaluate_all(self,words)
                            sys.stdout.flush()

                    #undo batch to save RAM
                    for i in batch:
                        i[0].representation = None
                        i[1].representation = None
                        i[0].unpopulate_embeddings()
                        i[1].unpopulate_embeddings()

                    #print 'Epoch ', (eidx+1), 'Update ', (uidx+1), 'Cost ', cost

                if(params.save):
                    counter += 1
                    self.saveParams(params.outfile+str(counter)+'.pickle')

                if(params.evaluate):
                    evaluate_all(self,words)

                print 'Epoch ', (eidx+1), 'Cost ', cost
                sys.stdout.flush()

        except KeyboardInterrupt:
            print "Training interupted"

        end_time = time.time()
        print "total time:", (end_time - start_time)

    def testFunction(self, g1x, mask, X):
        mat = self.allparams[0].get_value()
        n = int(np.sqrt(mat.shape[1]))
        X2 = np.zeros(X.shape)
        for i in range(g1x.shape[0]):
            prod = np.eye(n)
            for j in range(len(g1x[i,:])):
                if mask[i,j] > 0:
                    w = mat[g1x[i,j],:]
                    w = np.reshape(w,((n,n)))
                    prod = np.dot(prod,w)
            X2[i,:] = np.reshape(prod,(n*n,))
        print "X2: ",X2
        print "X: ",X